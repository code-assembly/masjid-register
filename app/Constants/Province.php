<?php

namespace App\Constants;

class Province
{
    const EASTERN_CAPE = 'Eastern Cape';
    const FREE_STATE = 'Free State';
    const GAUTENG = 'Gauteng';
    const KWA_ZULU_NATAL = 'KwaZulu-Natal';
    const LIMPOPO = 'Limpopo';
    const MPUMALANGA = 'Mpumalanga';
    const NORTH_WEST = 'North West';
    const NORTHERN_CAPE = 'Northern Cape';
    const WESTERN_CAPE = 'Western Cape';

    static function list()
    {
        return [
            self::EASTERN_CAPE,
            self::FREE_STATE,
            self::GAUTENG,
            self::KWA_ZULU_NATAL,
            self::LIMPOPO,
            self::MPUMALANGA,
            self::NORTH_WEST,
            self::NORTHERN_CAPE,
            self::WESTERN_CAPE,
        ];
    }
}
