<?php

namespace App\Http\Controllers;

use App\Constants\Province;
use App\Http\Requests\RegisterUserRequest;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function showRegistrationForm()
    {
        return view('user.registration_form');
    }

    public function registerUser(RegisterUserRequest $request)
    {
    }
}
