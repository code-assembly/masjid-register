<?php

namespace App\Http\Requests;

use App\Constants\Province;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            // Contact details
            'full_name' => ['required', 'max:255'],
            'mobile' => ['required_without:email', 'nullable', 'regex:/^(\+?)([\d ]*)$/'],
            'email' => ['required_without:mobile,null', 'nullable', 'email'],
            'contact_method' => ['required_with_all:mobile,email', 'in:mobile,email'],

            // Secondary Contact
            'secondary_contact_full_name' => ['nullable', 'max:255'],
            'secondary_contact_number' => ['nullable', 'regex:/^(\+?)([\d ]*)$/'],

            // Residential Address
            'building' => ['nullable', 'max:255'],
            'street_address' => ['required', 'max:255'],
            'suburb' => ['nullable', 'max:255'],
            'city' => ['nullable', 'max:255'],
            'province' => ['required', 'in:' . implode(',', Province::list())],
            'postal_code' => ['required', 'regex:/^\d{4}$/'],

            // Agreements

            'confirm_medical' => ['required', 'accepted'],
            'accept_risk' => ['required', 'accepted'],
            'accept_limited_liability' => ['required', 'accepted'],
        ];
    }

    public function messages()
    {
        return [
            // Contact details

            'full_name.required' => 'Your full name is required',
            'full_name.max'  => 'A maximum of 255 characters is allowed',

            'mobile.required_without'  => 'Your Mobile number or E-Mail address is required so we can reach you',
            'mobile.regex'  => 'Mobile number seems invalid',

            'email.required_without'  => 'Your E-Mail address or Mobile number is required so we can reach you',
            'email.email'  => 'E-Mail address seems invalid',

            'contact_method.required_with_all'  => 'Select your preferred contact method',
            'contact_method.in'  => 'Select your preferred contact method',

            // Residential Address

            'building.max' => 'A maximum of 255 characters is allowed',

            'street_address.required' => 'A street Address is required',
            'street_address.max' => 'A maximum of 255 characters is allowed',

            'suburb.max' => 'A maximum of 255 characters is allowed',

            'city.max' => 'A maximum of 255 characters is allowed',

            'province.required' => 'Select a province',
            'province.in' => 'Select a province',

            'postal_code.required' => 'A Postal Code is required',
            'postal_code.regex' => 'The postal address seems invalid',

            // Secondary Contact
            'secondary_contact_full_name.max' => 'A maximum of 255 characters is allowed',,
            'secondary_contact_number.regex' => 'The contact number seems invalid',
        ];
    }

    function getContactInfo()
    {
        return [
            'full_name'       =>  $this->input('full_name'),
            'mobile'          => $this->input('mobile'),
            'email'           =>  $this->input('email'),
            'contact_method'  => $this->input('contact_method'),
        ];
    }

    function getResidentialAddressJSON()
    {
        return json_encode([
            'building'          =>  $this->input('building'),
            'street_address'    => $this->input('street_address'),
            'suburb'            =>  $this->input('suburb'),
            'city'              => $this->input('city'),
            'province'          => $this->input('province'),
            'postal_code'       => $this->input('postal_code')
        ]);
    }
    function getSecondaryContactJSON()
    {
        return json_encode([
            'secondary_contact_full_name'          =>  $this->input('secondary_contact_full_name'),
            'secondary_contact_number'    => $this->input('secondary_contact_number'),
        ]);
    }
}
