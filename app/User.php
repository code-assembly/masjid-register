<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public $incrementing = false;
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->{$post->getKeyName()} = (string) Str::uuid();
        });
    }

    protected $guarded = [
        'email_verified_at', 'mobile_verified_at', 'deleted_at',
    ];

    protected $hidden = [
        'remember_token',
        'mobile_verified_at',
        'email_verified_at'
    ];
}
