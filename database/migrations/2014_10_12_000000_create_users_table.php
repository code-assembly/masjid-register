<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->string('full_name');

            $table->string('mobile')->nullable();
            $table->string('mobile_verified_at', 0)->nullable();

            $table->string('email')->nullable();
            $table->timestamp('email_verified_at', 0)->nullable();

            $table->string('contact_method', 24); //, ['mobile', 'email']);

            $table->json('residential_address');
            $table->json('secondary_contact');

            $table->string('status', 24)->default('created'); //, ['created', 'verified', 'blocked']);

            $table->boolean('confirm_medical');
            $table->boolean('accept_risk');
            $table->boolean('accept_limited_liability');

            $table->rememberToken();
            $table->timestamps(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
