<div class="form-group">
    <label for="building">Complex, Estate or Building (Optional)</label>
    <input
        name="building"
        type="text"
        id="building"
        value="{{ old('building') }}"
        class="form-control @error('building') is-invalid @enderror"
        aria-describedby="buildingHelp"
    />
    <small id="buildingHelp" class="form-text text-muted">
        Complex, Estate or Building Name including unit number or floor
    </small>
    @error('building')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="streetAddress">Street Address</label>
    <input
        name="street_address"
        type="text"
        id="streetAddress"
        value="{{ old('street_address') }}"
        class="form-control @error('street_address') is-invalid @enderror"
        aria-describedby="streetAddressHelp"
        required
    />
    <small id="streetAddressHelp" class="form-text text-muted">
        Eg. 3 Straight Road
    </small>
    @error('street_address')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-row">
    <div class="col-md-6 col-sm-12 mb-3">
        <label for="suburb">Suburb</label>
        <input
            name="suburb"
            type="text"
            id="suburb"
            value="{{ old('suburb') }}"
            class="form-control @error('suburb') is-invalid @enderror"
        />
        @error('suburb')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>

    <div class="col-md-6 col-sm-12 mb-3">
        <label for="city">City / Town</label>
        <input
            name="city"
            type="text"
            id="city"
            value="{{ old('city') }}"
            class="form-control @error('city') is-invalid @enderror"
        />
        @error('city')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="form-group"></div>

<div class="form-row">
    <div class="col-md-9 col-sm-12 mb-3">
        <label for="province">Province</label>
        <select
            name="province"
            id="province"
            class="custom-select @error('province') is-invalid @enderror"
            required
        >
            <option selected disabled value="">Choose...</option>
            @foreach (Province::list() as $province)
            <option value="{{ $province }}">{{ $province }}</option>
            @endforeach
        </select>

        @error('province')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-md-3 mb-3">
        <label for="postalCode">Postal Code</label>
        <input
            name="postal_code"
            type="text"
            id="postalCode"
            value="{{ old('postal_code') }}"
            pattern="\d{4}"
            maxlength="4"
            class="form-control @error('postal_code') is-invalid @enderror"
            required
        />
        @error('postal_code')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
