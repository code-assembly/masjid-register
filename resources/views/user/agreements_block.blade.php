<div class="form-group form-check">
    <input
        name="confirm_medical"
        type="checkbox"
        id="confirmMedical"
        class="form-check-input @error('confirm_medical') is-invalid @enderror"
        required
    />
    <label class="form-check-label" for="confirmMedical">
        I am not a part of an at <a>risk group</a> and do not display the
        following symptoms: Fever Sore throat, Shortness of breath, Difficulty
        in breathing, Body aches, Loss of smell or taste, Nausea / vomiting,
        Diarrhoea, Fatigue, Weakness or tiredness
    </label>
</div>

<div class="form-group form-check">
    <input
        name="accept_risk"
        type="checkbox"
        id="acceptRisk"
        class="form-check-input @error('accept_risk') is-invalid @enderror"
        required
    />
    <label class="form-check-label" for="acceptRisk">
        I understand the risks of contracting CORONA VIRUS 19 by attending
        congregational activities
    </label>
</div>

<div class="form-group form-check">
    <input
        name="accept_limited_liability"
        type="checkbox"
        id="acceptLimitedLiability"
        class="form-check-input @error('accept_limited_liability') is-invalid @enderror"
        required
    />
    <label class="form-check-label" for="acceptLimitedLiability">
        I hereby acknowledge that I accept and will fully comply with and abide
        by the NOTICES, RULES, INDEMNITIES, COMMON COURTESIES, OBLIGATIONS AND
        CAUTIONS and without reserve. I enter these premises AND participate in
        any or all activities at my own risk and indemnify and absolve the Trust
        Board, its Members, Management and staff members of any damage or loss
        to my/our personal property,physical injury or death
    </label>
</div>
