<div class="form-group">
    <label for="fullName">Full Name</label>
    <input
        name="full_name"
        type="text"
        id="fullName"
        value="{{ old('full_name') }}"
        maxlength="255"
        class="form-control @error('full_name') is-invalid @enderror"
    />
    @error('full_name')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-row">
    <div class="col-sm-12 col-md-6">
        <label for="mobile">Mobile Number</label>
        <input
            name="mobile"
            type="tel"
            id="mobile"
            value="{{ old('mobile') }}"
            pattern="^(\+?)([\d ]*)"
            class="form-control @error('mobile') is-invalid @enderror"
            aria-describedby="mobileHelp"
        />
        <small id="mobileHelp" class="form-text text-muted">
            A valid Mobile number or E-Mail address where you can be contacted.
        </small>
        @error('mobile')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-sm-12 col-md-6">
        <label for="email">E-Mail address</label>
        <input
            name="email"
            type="email"
            id="email"
            value="{{ old('email') }}"
            class="form-control @error('email') is-invalid @enderror"
            aria-describedby="emailHelp"
        />
        <small id="emailHelp" class="form-text text-muted">
            A valid E-Mail address or Mobile number where you are reachable
        </small>
        @error('email')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>

<div class="form-group mt-3">
    <label class="d-block">Preferred contact method</label>
    <div class="custom-control custom-radio custom-control-inline">
        <input
            name="contact_method"
            type="radio"
            id="contact_method_mobile"
            value="mobile"
            class="custom-control-input @error('contact_method') is-invalid @enderror"
        />
        <label class="custom-control-label" for="contact_method_mobile"
            >Mobile (SMS, WhatsApp, Call)</label
        >
    </div>
    <div class="custom-control custom-radio custom-control-inline">
        <input
            name="contact_method"
            type="radio"
            id="contact_method_email"
            value="email"
            class="custom-control-input @error('contact_method') is-invalid @enderror"
        />
        <label class="custom-control-label" for="contact_method_email"
            >Email</label
        >
    </div>
    <small class="form-text text-muted">
        We will use this method should we need to send out communications,
        updates or notifications
    </small>
    @error('contact_method')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
