@extends('index') @section('title', 'Register') @section('main')
<div class="container py-4">
    <h1 class="text-center">﷽</h1>
    <small class="text-center d-block text-muted"
        >In the Name of Allah, the Most Beneficent, the Most Merciful</small
    >

    <h2 class="mt-5">Registration</h2>
    <h3>Purpose of registration</h3>
    <p>
        Purpose of registration, safety, legislation, ease of access and
        removing the burden on musallies
    </p>

    <h3>Commitment to privacy</h3>
    <p>
        We will not be sharing any personally identifiable information with any
        third parties. this
    </p>

    <form method="POST" action="/user/register">
        @csrf

        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card h-100 mb-3">
                    <div class="card-body">
                        <h3 class="card-title">1. Contact Details</h3>
                        @include('user.contact_details_block')
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <h3 class="card-title">2. Residential Address</h3>
                        @include('user.address_block')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card h-100">
                    <div class="card-body">
                        <h3 class="card-title">3. Secondary Contact</h3>
                        @include('user.secondary_contact_block')
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">4. Agreements</h3>
                        @include('user.agreements_block')
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Register me</button>
    </form>
</div>
@endsection
