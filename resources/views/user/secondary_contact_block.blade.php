<div class="form-group">
    <label for="secondaryContactFullName">Emergency Contact Full Name</label>
    <input
        name="secondary_contact_full_name"
        type="text"
        id="secondaryContactFullName"
        maxlength="255"
        value="{{ old('secondary_contact_full_name') }}"
        class="form-control @error('secondary_contact_full_name') is-invalid @enderror"
        aria-describedby="secondaryContactFullNameHelp"
    />
    <small id="secondaryContactFullNameHelp" class="form-text text-muted">
        The name of an emergency contact that resides as the same residential
        address
    </small>
    @error('secondary_contact_full_name')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="secondaryContactNumber"
        >Emergency Contact Telephone Number</label
    >
    <input
        name="secondary_contact_number"
        type="tel"
        id="secondaryContactNumber"
        value="{{ old('secondary_contact_number') }}"
        pattern="^(\+?)([\d ]*)"
        class="form-control @error('secondary_contact_number') is-invalid @enderror"
        aria-describedby="secondaryContactNumberHelp"
    />
    <small id="secondaryContactNumberHelp" class="form-text text-muted">
        A valid Mobile or land line number where your emergency contact can be
        reached
    </small>
    @error('secondary_contact_number')
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
